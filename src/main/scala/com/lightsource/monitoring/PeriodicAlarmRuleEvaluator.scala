package com.lightsource.monitoring

import org.vertx.scala.platform.Verticle
import org.apache.http.impl.client.HttpClients
import org.apache.http.client.methods.HttpPut
import org.apache.http.client.methods.HttpGet
import org.apache.http.entity.StringEntity
import org.json4s.native.JsonParser
import org.json4s.native.JsonMethods._
import org.json4s._
import org.json4s.JsonDSL._
import scala.io.Source
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.DateTime
import com.typesafe.config.ConfigFactory
import com.typesafe.config.impl.SimpleConfigList
import com.typesafe.config.impl.SimpleConfigList
import com.typesafe.config.ConfigValue

class PeriodicAlarmRuleEvaluator extends Verticle {

  override def start() = {
    
    aslNoDataRuleEvaluation()

    val logger = container.logger()

    val periodicTimeId: Long = vertx.setPeriodic(10000, id => {

      logger.info("Triggering rule evaluation")
    })
  }

  def aslNoDataRuleEvaluation() = {

    lazy val timeOfLastReading = {
      val q = "http://localhost:8080/meterdb/aslreadings?sort_by=-timestamp&pagesize=1"

      val httpCmd = new HttpGet(q)
      val client = HttpClients.createDefault()

      httpCmd.setHeader("Accept", "application/json")
      httpCmd.setHeader("Content-type", "application/json")

      val response = client.execute(httpCmd)

      val is = response.getEntity.getContent
      val content = Source.fromInputStream(is).getLines().mkString

      val jsonVal = JsonParser.parse(content) \\ "timestamp"

      val dateString = jsonVal match {
        case JString(s) => s
        case _          => throw new RuntimeException("did not find any readings")
      }
      ISODateTimeFormat.dateTime().parseDateTime(dateString)
    }

    if (DateTime.now().minusHours(33).isAfter(timeOfLastReading)) raiseNoDataAlarm(timeOfLastReading)

  }
  
  def raiseNoDataAlarm(timeOfLastReading: DateTime) =  {
    
      val newJson: JValue =
            ("type" -> "NoData") ~
            ("meterId" -> "EML1206003695") ~
              ("from" -> "2015-03-09T23:30:00.000Z") ~
              ("to" -> null)

      val url = "http://localhost:8080/lscrmdbalarm......"
      val httpCmd = new HttpPut(url)

      httpCmd.setEntity(new StringEntity(compact(render(newJson))));
      val client = HttpClients.createDefault();

      httpCmd.setHeader("Accept", "application/json")
      httpCmd.setHeader("Content-type", "application/json")

      val response = client.execute(httpCmd)    
    
  }

}